<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>E-Comerce</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/custom.css"/>
</head>
<body>

<div class="container">
    <div class="title">
        <h2 >E-Comerce</h2>

    <hr/>
        <div class="bg-light clearfix">
            <div class="pull-left">  <h3>Products</h3></div>
            <div class="pull-right"> <a href="/cart" ><h3>Cart</h3></a></div>
        </div>


        </h3>
    </div>
    <hr/>

    <div>
        <table class="table" style="width:100%">
            <thead class="thead-light">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Description</th>
                <th scope="col">Amount</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="cart_products" items="${products}">
                <tr>
                    <th scope="row">${cart_products.id}</th>
                    <td>${cart_products.description}</td>
                    <td>${cart_products.amount}</td>
                    <th><a href="buy?id=${cart_products.id}" class="glyphicon glyphicon-plus" style="font-weight: bold ">Add to cart</a></th>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>

</body>
</html>
