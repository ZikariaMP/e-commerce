package com.example.Test.controller;

import com.example.Test.models.Products;
import com.example.Test.services.ProductsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.*;

@Controller
@EnableScheduling
public class MainController {


	@Autowired
	private ProductsServices productsServices;


	//Estas funciones son globales para poder utilizar en diferentes funciones.
	private ScheduledFuture<?> future = null;
	private ArrayList<Products> cart = new ArrayList<Products>();

	//Esta funcion devuelve la pagina principal con todos los productos para poder comprar.
	@GetMapping("/")
	public String getAllProducts(HttpServletRequest req){
		req.setAttribute("products",productsServices.findAllProducts());
		return "productsView";
	}

	//Esta funcion para el temporizador para eliminar todos los productos del cart.
	public void stopTTL(){
		if(future!=null && !future.isCancelled()) {
			future.cancel(true);
		}
	}

	//Esta funcion añade el producto al cart.
	@GetMapping("/buy")
	public String buyProduct(@RequestParam int id){
		cart.add(productsServices.buyProduct(id));

		stopTTL();

		return "redirect:/cart";

	}

	//Esta funcion se ejecuta cuando le das a boton de comprar
	@GetMapping("/result")
	public String buyCart(HttpServletRequest req){
		req.setAttribute("msg","Thanks for shopping on our website");
		return "resultView";
	}

	//Esta funcion elimina los datos del cart.
	public String deleteCart(){
			cart.clear();
		return "redirect:/cart";
	}

	//Esta funcion es para volver a la pagina principal despues de comprar una cesta.
	@GetMapping("/finished")
	public String shoppingDone(){
		cart.clear();
		return "redirect:/";
	}

	//Esta funcion develve el cart con los productos que has añadido para comprar
	@GetMapping("/cart")
	public String viewCart(HttpServletRequest req){
		float total_price=0;
		ScheduledExecutorService service;

		// el temporizador es null o esta cancelado lo empieza con 10minutos y luego ejecuta la funcion deleteCart()
		//La condicion de future==null es para que entre la primera vez ya que tiene un valos null, y dentro del if se genera.
		if ((future == null || future.isCancelled()) && !cart.isEmpty()){
			service = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
			future =service.scheduleWithFixedDelay(this::deleteCart, 10, 10, TimeUnit.MINUTES);
		}

		//Recorro el cart para coger el precio de cada producto y sumarlo al precio total
		for (Products products : cart) {
			total_price += Float.parseFloat(products.getAmount());
		}

		req.setAttribute("cart_products",cart);
		req.setAttribute("total_price",total_price);

		//Envio el tiempo de TTL si esta en progreso y sino envio de que no esta activo.
		if(!cart.isEmpty()) {
			req.setAttribute("ttl", future.getDelay(TimeUnit.MINUTES) +" minutes");
		}
		else
		{
			stopTTL();
			req.setAttribute("ttl", "Not in progress");
		}
		return "cartView";
	}

	//En esta funcion elimino X producto que seleccionas del cart para eliminar.
	@GetMapping("/delete")
	public String delelteCartProduct(@RequestParam int id){
		if(cart.size()!=1)
		{
			for (int i = 0; i < cart.size(); i++) {
				if (cart.get(i).getId() == id) {
					cart.remove(i);
				}
			}
		}
		else {
			cart.remove(0);
		}
		return "redirect:/cart";
	}

}
