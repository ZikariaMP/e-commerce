package com.example.Test.services;

import com.example.Test.dao.ProductsDAO;
import com.example.Test.models.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

@Service
public class ProductsServices {
    @Autowired
    private ProductsDAO productsDAO;

    //Esta funcion me devuelve todos los productos.
    public ArrayList<Products> findAllProducts(){
        ArrayList<Products> products =new ArrayList<>();

        for (Products p : productsDAO.findAll()){
            products.add(p);
        }
        return products;
    }

    //Esta funcion me devuelve el producto deseado, dada la id.
    public Products buyProduct(int id){
        Products product=null;
        for (Products p : productsDAO.findAllById(Collections.singleton(id))){
            product=p;
        }
        return  product;
    }


}
