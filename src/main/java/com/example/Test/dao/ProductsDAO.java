package com.example.Test.dao;

import com.example.Test.models.Products;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsDAO extends CrudRepository<Products, Integer> {
}
